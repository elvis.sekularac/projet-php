<?php

// Fonction qui affiche la page de base quand on n'est pas connecté sur le site
function anonyme(){
  require ("modele/accueil.php");
  $contenu = 'affichageVoiture';
  accueil($contenu);
}

// Fonction qui affiche la page de base quand on est connecté sur le site
function connecte(){
  require ("modele/accueil.php");
  $contenu = 'affichageVoiture';
  accueil($contenu);
}

// Fonction qui permet d'inscrire une entreprise dans la base de données
function inscription(){
  $nom = isset($_POST['nom'])?trim($_POST['nom']):'';
  $mdp = isset($_POST['mdp'])?trim($_POST['mdp' ]):'';
  $mdp2 = isset($_POST['mdp2'])?trim($_POST['mdp2']):'';
  $email = isset($_POST['email'])?trim($_POST['email']):'';
  $email2 = isset($_POST['email2'])?trim($_POST['email2']):'';
  $err = '';
  $_SESSION['msg'] = '';

  $contenu = 'inscription';
  require ("modele/accueil.php");
  require ("modele/verifInscription.php");

  if (count($_POST)==0){
    accueil($contenu);
  }

  else{
      // On vérifie que les données entrées sont valides
      if (($bool=verif_ident($nom, $mdp,$mdp2, $email,$email2, $err)) && !verif_inscription($mdp, $email)) {
        require ('modele/inscription.php');
        inscriptionBDD($nom,$mdp,$email);

        // Si l'inscription est un succès on connecte l'entreprise
        require ('modele/connexion.php');
        connexionBDD($mdp,$email,$profil);
        $_SESSION['profil'] = $profil;
        header("Location: index.php?controle=entreprise&action=connecte");
      }
      else {
        if (!$bool) {
          $mdp = '';
          $mdp2 = '';
          $email2 = '';
          $msg = $err;
        }
        else $msg = "Utilisateur déjà existant !";
        $_SESSION['msg'] = $msg;
        accueil($contenu);
      }
  }
}

// Fonction qui permet de connecter une entreprise ou l'admin au site
function connexion(){
    $email = isset($_POST['email'])?trim($_POST['email']):'';
    $mdp = isset($_POST['mdp'])?trim($_POST['mdp']):'';
    $profil = '';
    $_SESSION['msg'] = '';
    $contenu = "connexion";
    require ("modele/accueil.php");
    require ("modele/verifInscription.php");
    if (count($_POST)==0){
      accueil($contenu);
    }
    else{
        if (verif_inscription($mdp, $email)) {
            require ('modele/connexion.php');
            connexionBDD($mdp,$email,$profil);
            $_SESSION['profil'] = $profil;
            header("Location: index.php?controle=entreprise&action=connecte");
        }
        else {
            $mdp = '';
            $email = '';
            $msg = "Vos identifiants de connexion ne correspondent à aucun compte sur notre système.";
            $_SESSION['msg'] = $msg; 
            accueil($contenu);
        }
    }
}

// Fonction qui permet de deconnecter l'utilisateur
function deconnexion(){
    session_destroy();
    header("Location: index.php?controle=entreprise&action=anonyme");
}


// Fonction qui vérifie la validité des données de l'inscription
function verif_ident(&$nom, $mdp,$mdp2, &$email,$email2, &$err) {
  $bool = true;
  if (!preg_match("`^[[:alpha:]\-]{1,30}$`", $nom)) {
      $nom = "";
      $err .= 'erreur de syntaxe sur le nom /';
      $bool = false;
  }
  if (!preg_match("`^[[:alpha:][:digit:]\-]{1,30}$`", $mdp)) {
      $err .= 'erreur de syntaxe sur le mot de passe /';
      $bool = false;
  }
  if (!filter_var($email, FILTER_VALIDATE_EMAIL)) {
      $email = "";
      $err .= 'erreur de syntaxe sur l\'email /';
      $bool = false;
  }
  if(!(strcmp($email,$email2)==0)){
      $email = "";
      $err .= 'erreur : les deux emails ne sont pas similaires /';
      $bool = false;
  }
  if(!(strcmp($mdp,$mdp2)==0)){
      $mdp = "";
      $err .= 'erreur : les deux mots de passe ne sont pas similaires /';
      $bool = false;
  }
  return $bool;
}

?>
