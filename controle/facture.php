<?php
	function formPaye(){
		$_SESSION['idE']= isset($_POST['idE'])?($_POST['idE']):'';
		require("modele/accueil.php");
		$contenu = 'paiement';
		accueil($contenu);
	}

	// Fonction qui permet de payer la facture lié à une location
	function payer(){
		$idE=  isset($_POST['idE'])?($_POST['idE']):'';
		$carte=  isset($_POST['carte'])?($_POST['carte']):'';
		$num=  isset($_POST['num'])?($_POST['num']):'';
		$cvv=  isset($_POST['cvv'])?($_POST['cvv']):'';
		$nom=  isset($_POST['nom'])?($_POST['nom']):'';
		$mois=  isset($_POST['mois'])?($_POST['mois']):'';
		$annee=  isset($_POST['annee'])?($_POST['annee']):'';
		$err="";
		$_SESSION['msg'] = '';
		$contenu = "paiement";
		require ("modele/accueil.php");
		if (count($_POST)==0){
			accueil($contenu);
		}
		else{
			// Vérifie toute les données entrées
			if(verif_paie($carte, $num, $cvv, $nom, $mois, $annee, $err)){
				require ("modele/payerBDD.php");
				payerBDD($idE);
				$contenu= "facture";
				accueil($contenu);
			}
			else {
				$_SESSION['msg'] = $err;
				accueil($contenu);
			}
		}
	}

	// Fonction qui permet de vérifier la validité des données entrées
	function verif_paie(&$carte, &$num, &$cvv, &$nom, &$mois, &$annee, &$err){
			$bool = true;
			if (!preg_match("`^[[:alpha:][:digit:]\-]{1,30}$`", $carte)) {
				$type = '';
				$err .= 'type de carte non choisi /';
				$bool = false;
			}
			if (!preg_match("`^[[:digit:]\-]{16}$`", $num)) {
				$mot = '';
				$err .= 'erreur de syntaxe sur le numero de carte /';
				$bool = false;
			}
			if (!preg_match("`^[[:digit:]\-]{3}$`", $cvv)) {
				$vit = '';
				$err .= 'erreur de syntaxe sur le cvv /';
				$bool = false;
			}
			if (!preg_match("`^[[:alpha:][:digit:]\-]{1,30}$`", $nom)) {
				$pl = '';
				$err .= 'erreur de syntaxe sur le nom /';
				$bool = false;
			}
			if (!preg_match("`^[[:digit:]\-]{1,2}$`", $mois)) {
				$pr = '';
				$err .= 'mois invalide/';
				$bool = false;
			}
			if (!preg_match("`^[[:digit:]\-]{4}$`", $annee)) {
				$pr = '';
				$err .= 'année invalide /';
				$bool = false;
			}
			return $bool;
	}


	function facturerA(){
		require("modele/accueil.php");
		$contenu = 'factureLoueur';
		accueil($contenu);
	}
	function factureLoueur(){
		require ("modele/factureAdmin.php");
		return factureAdmin();
	}
	function facturerE(){
	  	require("modele/accueil.php");
	 	$contenu = 'facture';
	  	accueil($contenu);
	}
    function factureEntreprise(){
		$ide= isset($_SESSION['client']['idE'])?($_SESSION['client']['idE']):$_SESSION['profil']['ide'];
		require ("modele/factureParticulier.php");
		return factureParticulier($ide);
		
	}
	
	function facturerEL(){
		$_SESSION['client']['idE']=  isset($_POST['idE'])?($_POST['idE']):'';
		$_SESSION['client']['nom']=  isset($_POST['nom'])?($_POST['nom']):'';
		$_SESSION['client']['email']=  isset($_POST['email'])?($_POST['email']):'';
	  	require("modele/accueil.php");
	 	$contenu = 'factureELoueur';
	  	accueil($contenu);
	}
	
	// Permet de louer une voiture
	function location(){
		date_default_timezone_set('Europe/Paris');
		$dateD=  isset($_POST['dateD'])?($_POST['dateD']):'';
		$dateF=  isset($_POST['dateF'])?($_POST['dateF']):'';
		$idV=  isset($_POST['idV'])?($_POST['idV']):'';
		$valeur=  isset($_POST['valeur'])?($_POST['valeur']):'';
		$pr=  isset($_POST['prix'])?($_POST['prix']):'';
		$idE= $_SESSION['profil']['ide'];
		$etat=0;
		require("modele/accueil.php");
	 	$contenu = 'louerVoiture';
		if (count($_POST)==0){
			accueil($contenu);
		}
		// Vérifie que la date de fin est supérieure à la date de début
		if($dateD<$dateF){
			require ("modele/creationFacture.php");
			creationFacture($idE, $idV, $dateD, $dateF, $valeur, $etat);
			$contenu = 'affichageVoiture';
			accueil($contenu);
		}
		else {
			$msg="Les dates doivent être postérieur à celle d'aujourd'hui et la date de fin doit être postérieure à celle de de début.";
			$_SESSION['msg'] = $msg;
			accueil($contenu);
		}
	}
	// Met un véhicule à l'état de loué en inserant l'id du loueur a la place de "disponible"
	function loue($idv,$ide){
		require ("modele/louerVoiture.php");
		louerVoiture($idv,$ide);
	}
?>