<?php
	function choisi(){
		$_SESSION['idV']= isset($_POST['idV'])?($_POST['idV']):'';
	  	require("modele/accueil.php");
	 	$contenu = 'louerVoiture';
	  	accueil($contenu);
	}

	// Permet d'ajouter une voiture dans la base de données
	function ajout() {
		$type=  isset($_POST['type'])?($_POST['type']):'';
		$mot=  isset($_POST['moteur'])?($_POST['moteur']):'';
		$vit=  isset($_POST['vitesse'])?($_POST['vitesse']):'';
		$pl=  isset($_POST['places'])?($_POST['places']):'';
		$pr=  isset($_POST['prix'])?($_POST['prix']):'';
		$err="";
		$_SESSION['msg'] = '';
		$contenu = "ajoutVoiture";

		require ("modele/accueil.php");
		if (count($_POST)==0){
			accueil($contenu);
		}
		else{
			$dossier = 'vue/image/';
			$fichier = $dossier . basename($_FILES["photo"]["name"]);
			$imageFileType = strtolower(pathinfo($dossier,PATHINFO_EXTENSION));
			move_uploaded_file($_FILES["photo"]["tmp_name"], $fichier);
			$photo=$_FILES["photo"]["name"];
			// Vérifie qu'une image a été deposée
			if (empty($photo)){
				$err .= 'pas de photo';
				$_SESSION['msg'] = $err;
				accueil($contenu);
			}
			// Vérifie toute les données entrées
			if(verif_ajout($type, $mot, $vit, $pl, $pr, $err)){
				$c = (object)array("moteur" => $mot, "vitesse" => $vit, "places" => $pl, "prix" => $pr);
				$caract = json_encode($c);
				$location="disponible";
				require ("modele/ajouterVoiture.php");
				ajouterVoiture($type,$caract,$location,$photo);
				accueil($contenu);
			}
			
			else {
				$_SESSION['msg'] = $err;
				accueil($contenu);
			}
		}
	}

	// Permet à l'admin de supprimer un véhicule de la base de données
	function retire() {
		$idV=  isset($_POST['idV'])?($_POST['idV']):'';
		require ("modele/retirerVehicule.php");
		retirerVehicule($idV);
		stock();
	}
	function stock(){
		require ("modele/accueil.php");
		$contenu = "stock";
		accueil($contenu);
	}

	// Permet d'afficher toutes les voitures (donc le stock de l'admin)
	function affStock() {
		require ("modele/affichageStock.php");
		return affichageStock();
	}

	// Permet d'afficher toutes les voitures actuellement louables
	function affichage() {
		require ("modele/affichageVoitureDispo.php");
		return affichageVoitureDispo();
	}

	// Vérifie la syntaxe des données qui ont été rentrés par l'admin lors de l'ajout d'une voiture
	function verif_ajout(&$type, &$mot, &$vit, &$pl, &$pr, &$err){
			$bool = true;
			if (!preg_match("`^[[:alpha:][:digit:]\-]{1,30}$`", $type)) {
				$type = '';
				$err .= 'erreur de syntaxe sur le type /';
				$bool = false;
			}
			if (!preg_match("`^[[:alpha:][:digit:]\-]{1,30}$`", $mot)) {
				$mot = '';
				$err .= 'erreur de syntaxe sur le moteur /';
				$bool = false;
			}
			if (!preg_match("`^[[:alpha:][:digit:]\-]{1,30}$`", $vit)) {
				$vit = '';
				$err .= 'erreur de syntaxe sur la vitesse /';
				$bool = false;
			}
			if (!preg_match("`^[[:digit:]\-]{1}$`", $pl)) {
				$pl = '';
				$err .= 'erreur de syntaxe sur le nombre de places /';
				$bool = false;
			}
			if (!preg_match("`^[[:digit:]\-]{1,5}$`", $pr)) {
				$pr = '';
				$err .= 'erreur de syntaxe sur le prix /';
				$bool = false;
			}
			return $bool;
	}
	// Affiche la voiture que l'entreprise a souhaité louer
	function choix(){
		require ("modele/choixLocation.php");
		return choixLocation($_SESSION['idV']);
    }

	function locationEntreprise(){
		require ("modele/accueil.php");
		$contenu = 'affichageLocation';
		accueil($contenu);
	}

	// Affiche les locations de l'entreprise actuellement connecté
	function affichageLocations($ide){
		require ("modele/locationsConnecte.php");
		return locationsConnecte($ide);	
	}
	// Annule la location d'une entreprise
	function annuler(){
		// Change l'état de location de la voiture à "disponible"
		require ("modele/voitureDispo.php");
		voitureDispo();
		// Supprime la facture de la base de données
		require ("modele/retirerFacture.php");
		retirerFacture();
		// Affiche de nouveau les locations après le retrait
		locationEntreprise();
	}

?>
