<?php
// Fonction qui affiche le stock de l'admin
function affichageStock(){
    require ("modele/connexionSQL.php");
    $sql="SELECT * FROM vehicule";
    try {
        $commande = $pdo->prepare($sql);
        $bool=$commande->execute();
        $V = array();
        if ($bool) {
            while ($l = $commande->fetch()) {
                $V[] = $l;
            }
        }
    }
    catch (PDOException $e) {
        echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
        die(); // On arrête tout.
    }
    return $V;
}

?>