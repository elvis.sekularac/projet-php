<?php
// Fonction qui affiche toutes les voitures n'étant pas encore louées
function affichageVoitureDispo(){
    require ("modele/connexionSQL.php");
	$sql="SELECT * FROM vehicule WHERE location = 'disponible'";
	try {
		$commande = $pdo->prepare($sql);
		$bool=$commande->execute();
		$V = array();
		if ($bool) {
			while ($l = $commande->fetch()) {
				$V[] = $l;
			}
		}
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
	return $V;
}

?>