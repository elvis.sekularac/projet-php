<?php   
// Commande SQL qui une voiture dans la base de données
function ajouterVoiture($type,$caract,$location,$photo){
    require ("modele/connexionSQL.php");
	$sql="INSERT INTO vehicule (type, caract, location, photo) VALUES (:type, :caract, :location, :photo)";
	try {
		$commande = $pdo->prepare($sql);
		$commande->execute(array(':type' => $type, ':caract' => $caract, ':location' => $location, ':photo' => $photo));
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}
?>