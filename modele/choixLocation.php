<?php
// Fonction qui permet d'afficher le véhicule qui a été choisi pour être loué
function choixLocation($idV){
    require ("modele/connexionSQL.php");
    $sql="SELECT * FROM vehicule WHERE idv=:idv";
    try {
                    $commande = $pdo->prepare($sql);
                    $commande->bindParam(':idv', $idV, PDO::PARAM_STR);
                    $commande->execute();
                    $v = $commande->fetch();
    }
    catch (PDOException $e) {
        echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
        die(); // On arrête tout.
    }
    return $v;
}

?>