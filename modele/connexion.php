<?php
// Fonction qui connecte l'utilisateur
function connexionBDD($mdp,$email,&$profil){
  require('modele/connexionSQL.php');
  $sql="SELECT * FROM `entreprise` WHERE mdp=:mdp AND email=:email";
  $mdpCrypte = sha1($mdp);
  echo($mdpCrypte);
  try {
    $commande = $pdo->prepare($sql);
    $commande->bindParam(':mdp', $mdpCrypte);
    $commande->bindParam(':email', $email);
    $bool = $commande->execute();
    if ($bool)
        $resultat = $commande->fetchAll(PDO::FETCH_ASSOC);
  }
  catch (PDOException $e) {
    echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
    die();
  }
	if (count($resultat) == 0) {
		$profil = array();
		return false;
	}
	else {
		$profil = $resultat[0];
		return true;
	}
}


?>
