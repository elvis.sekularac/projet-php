<?php
	// Permet de se connecter et d'utiliser la base de données
	$hostname = "localhost";
	$base= "projetphp";
	$loginBD= "root";
	$passBD="root";

	try {
		$dsn = "mysql:server=$hostname ; dbname=$base";
		$pdo = new PDO ($dsn, $loginBD, $passBD, array(PDO::MYSQL_ATTR_INIT_COMMAND => "SET NAMES utf8"));
		// On active le mode d'affichage des erreurs, et le lancement d'exception en cas d'erreur
		$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de connexion : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
?>
