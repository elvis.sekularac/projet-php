<?php 
// Commande SQL qui crée une facture lorsqu'une voiture est louée
function creationFacture($idE, $idV, $dateD, $dateF, $valeur, $etat){
    require ("modele/connexionSQL.php");
	$sql="INSERT INTO facture (ide, idv, dateD, dateF, valeur, etat) VALUES (:ide, :idv, :dateD, :dateF, :valeur, :etat)";
	try {
		$commande = $pdo->prepare($sql);
        $commande->execute(array(':ide' => $idE, ':idv' => $idV, ':dateD' => $dateD, ':dateF' => $dateF, ':valeur' => $valeur, ':etat' => $etat));		
    	loue($idV,$idE);
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}

?>