<?php 
// Fonction qui renvoie toutes les factures présent dans la base de données
function factureAdmin(){
	require ("modele/connexionSQL.php");
	$sql="SELECT * FROM facture F INNER JOIN entreprise E ON F.ide=E.ide GROUP BY F.ide";
	try {
		$commande = $pdo->prepare($sql);
		$bool=$commande->execute();
		$F = array();
		if ($bool) {
			while ($l = $commande->fetch()) {
				$F[] = $l;
			}
		}
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
	return $F;
}

?>