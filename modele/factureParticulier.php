<?php 
// Fonction qui permet d'afficher la facture pour une entreprise en particulier
function factureParticulier($ide){
    require ("modele/connexionSQL.php");
    $sql="SELECT * FROM vehicule V INNER JOIN facture F ON V.idv = F.idv INNER JOIN entreprise E ON F.ide = E.ide WHERE F.ide=:idE";
    try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':idE', $ide, PDO::PARAM_STR);
		$bool=$commande->execute();
		$F = array();
		if ($bool) {
			while ($l = $commande->fetch()) {
				$F[] = $l;
			}
		}
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
   }
    return $F;
}
?>