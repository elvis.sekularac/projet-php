<?php
// Commande SQL qui inscrit une entreprise dans la base de données
function inscriptionBDD($nom,$mdp,$email){
  require('connexionSQL.php');
  $sql="INSERT INTO `entreprise` (nom,email,mdp) VALUES(:nom,:email,:mdp)";
  $mdpCrypte = sha1($mdp);
  try {
    $commande = $pdo->prepare($sql);
    $commande->bindParam(':nom', $nom);
    $commande->bindParam(':email', $email);
    $commande->bindParam(':mdp', $mdpCrypte);
    $bool = $commande->execute();
  }
  catch (PDOException $e) {
    echo utf8_encode("Echec de insert : " . $e->getMessage() . "\n");
    die();
  }
}

?>
