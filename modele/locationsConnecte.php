<?php
// Permet d'afficher la liste des véhicules loués par l'entreprise connecté
function locationsConnecte($ide){
    require ("modele/connexionSQL.php");
    $sql="SELECT * FROM vehicule V INNER JOIN facture F ON V.idv = F.idv WHERE F.ide=:ide";
    try {
        $commande = $pdo->prepare($sql);
        $commande->bindParam(':ide', $ide, PDO::PARAM_STR);
        $bool = $commande->execute();
        $V = array();
        if ($bool) {
            while ($l = $commande->fetch()) {
                $V[] = $l;
            }
        }
    }
    catch (PDOException $e) {
        echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
        die(); // On arrête tout.
    }
    return $V;
}

?>