<?php 
// Fonction qui permet de louer une voiture
function louerVoiture($idv,$ide){
    $sql="UPDATE vehicule SET location = :ide WHERE idv = :idv";
	require ("modele/connexionSQL.php");
	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':ide', $ide, PDO::PARAM_STR);
		$commande->bindParam(':idv', $idv, PDO::PARAM_STR);
		$commande->execute();
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
	}
}

?>