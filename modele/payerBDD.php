<?php
	// Fonction qui permet de proceder au paiement d'une facture
	function payerBDD($idE){
		require ("modele/connexionSQL.php");
		$etat=1;
		$sql="UPDATE facture SET etat = :etat WHERE ide = :ide";
		try {
			$commande = $pdo->prepare($sql);
			$commande->bindParam(':etat', $etat, PDO::PARAM_STR);
			$commande->bindParam(':ide', $idE, PDO::PARAM_STR);
			$commande->execute();
		}
		catch (PDOException $e) {
			echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
			die(); // On arrête tout.
		}
	}
?>