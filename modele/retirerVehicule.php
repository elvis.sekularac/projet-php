<?php 
// Fonction qui permet de retirer un vehicule de la base de données
function retirerVehicule($idV){
    require ("modele/connexionSQL.php");
    $sql="DELETE FROM vehicule  WHERE idv=:idv";
    try {
        $commande = $pdo->prepare($sql);
        $commande->bindParam(':idv', $idV, PDO::PARAM_STR);
        $commande->execute();
    }
    catch (PDOException $e) {
        echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
        die(); // On arrête tout.
    }
}

?>