<?php
	// Commande SQL qui vérifie si un utilisateur existe ou non
	function verif_inscription($mdp,$email) {
		require('connexionSQL.php');
		$sql="SELECT * FROM `entreprise` WHERE mdp=:mdp AND email=:email";
		$mdpCrypte = sha1($mdp);
		try {
			$commande = $pdo->prepare($sql);
			$commande->bindParam(':mdp', $mdpCrypte);
			$commande->bindParam(':email', $email);
			$bool = $commande->execute();
      		if ($bool) {
				$resultat = $commande->fetchAll(PDO::FETCH_ASSOC);
			}
		}
		catch (PDOException $e) {
			echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
			die();
		}
		if (count($resultat) == 0) {
			return false;
		}
		else {
			return true;
		}
	}
?>
