<?php 
// Commande SQL qui remet l'état d'une voiture à "disponible"
function voitureDispo(){
    $idV=  isset($_POST['idV'])?($_POST['idV']):'';
	require ("modele/connexionSQL.php");
	$sql="UPDATE vehicule SET location = 'disponible' WHERE idv=:idv";
	try {
		$commande = $pdo->prepare($sql);
		$commande->bindParam(':idv', $idV, PDO::PARAM_STR);
		$bool = $commande->execute();
	}
	catch (PDOException $e) {
		echo utf8_encode("Echec de select : " . $e->getMessage() . "\n");
		die(); // On arrête tout.
    }
}

?>