<html>
	<head>
		<meta charset="utf-8"/>
	</head>
	<body>
		<?php
			require_once("controle/voiture.php");
			$Voiture=affichageLocations($_SESSION['profil']['ide']);	
			if ($Voiture != false) {
		?>
				<div class='container-fluid'>
					<div class='row justify-content-around custom-line'>
						<?php
							foreach ($Voiture as $v) {
								$c= json_decode($v['caract']);
						?>
							<div class='col-lg-4 col-md-6 col-sm-12 col-12' id='colV' style='width: 18rem;'>
								<div class='card'>
									<img class='card-img-top' src='vue/image/<?php echo ($v['photo']);?>'>
										<div class='card-body'>
											<p class='card-text'>Type : <?php echo ($v['type']); ?> </p>
											<p class='card-text'>Caractérisque : moteur : <?php echo ($c->moteur); ?> <br> vitesse : <?php echo ($c->vitesse); ?> <br> places : <?php echo ($c->places); ?> <br> prix journalier : <?php echo ($c->prix); ?> €</p>
											<p class='card-text'>Etat : <?php echo ($v['location']); ?></p>
											<p class='card-text'>Date de début : <?php echo ($v['dateD']); ?></p>
											<p class='card-text'>Date de fin : <?php echo ($v['dateF']); ?></p>
										</div>
									<form action="index.php?controle=voiture&action=annuler" method="post" enctype="multipart/form-data">
										<input name="idV" type="hidden" value="<?php echo ($v['idv']); ?>">
										<input class="bt" type= "submit"  value="Annuler" name="submit">
									</form>
                                    </div>
							</div>
									<?php
										}
									?>

					</div>
				</div>
			<?php
        }
        else { ?> <h1 style="text-align: center"> Vous n'avez pas loué de véhicules </h1> <?php }?>
			
	</body>
</html>
