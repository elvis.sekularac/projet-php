<html>
	<head>
	</head>
	<body>
		<div id="formulaireVoiture">
			<h3> Ajout d'un véhicule </h3>
			<form action="index.php?controle=voiture&action=ajout" method="post" enctype="multipart/form-data">
				<table>
					<tr>
						<td class="attVoiture">
							<label>Type :</label>
						</td>
						<td>
							<input 	name="type" 	type="text" value= "" />
						</td>
					</tr>
					<tr>
						<td class="attVoiture">
							<label>Moteur :</label>
						</td>
						<td>
							<input  name="moteur"  type="text" value= "" />
						</td>
					</tr>
					<tr>
						<td class="attVoiture">
							<label>Vitesse :</label>
						</td>
						<td>
							<input  name="vitesse"  type="text" value= "" />
						</td>
					</tr>
					<tr>
						<td class="attVoiture">
							<label>Nombre de places :</label>
						</td>
						<td>
							<input  name="places"  type="text" value= "" />
						</td>
					</tr>
						<tr>
						<td class="attVoiture">
							<label>Prix journalier:</label>
						</td>
						<td>
							<input  name="prix"  type="text" value= "" />
						</td>
					</tr>
					<tr>
						<td class="attVoiture">
							<label>Photo :</label>
						</td>
						<td>
							<input  name="photo" id="photo" type="file" accept="image/png, image/jpeg, image/jpg" value= "" />
						</td>
					</tr>
				</table>
				<br>
				<input id="soumVoiture" type= "submit"  value="Soumettre" name="submit">
			</form>
		</div>
		<div align="center" color="red"> <?php if(isset($_SESSION['msg'])) echo $_SESSION['msg']; ?> </div>
	</body>
</html>
