<html>
	<head>
	</head>
	<body>
	<?php
		require_once("controle/facture.php");
		$Facture=factureEntreprise();
		date_default_timezone_set('Europe/Paris');
		if($Facture!=false)
		{
			$prixTotal=0;
			$cpt=0;
			$pay[] = array();
	?>
			<div id='cadreFac'>
				<div class="row contenueFac">
					<div class="col">
						<h1>Rent My Car</h1>
						<p>15 boulevard de la république</p>
						<p>75010, Paris</p>
						<p>01 05 03 05 04</p>
						<p>RentMyCar@outlook.com</p>
					</div>
					<div class="col" id="clientFac">
						<h2 id="fac">FACTURE</h2>
						<h1><?php echo $_SESSION['profil']['nom'];?></h1>
						<p><?php echo $_SESSION['profil']['email'];?></p>
					</div>
				</div>
				<div class="contenueFac">
					<table class="table" id="tableFac">
						<thead class="thead-dark">
							<tr>
							<th scope="col">Voiture</th>
							<th scope="col">Date Début</th>	
							<th scope="col">Date Fin</th>
							<th scope="col">Prix journalier</th>
							<th scope="col">Prix</th>
							</tr>
						</thead>
						<?php foreach($Facture as $f){
							$c= json_decode($f['caract']);
							$dateDeb = new DateTime($f['dateD']);
							$dateFin = new DateTime($f['dateF']);
							$interval = $dateDeb->diff($dateFin)->days;
							$prixL=$c->prix * $interval;
							$prixTotal+=$prixL;
							$cpt++;
							$etat=$f['etat'];
						?>
						<tbody>
							<tr>
							<th scope="row"><?php echo $f['type']; ?></th>
							<td><?php echo $f['dateD']; ?></td>
							<td><?php echo $f['dateF']; ?></td>
							<td><?php echo $c->prix . " €"; ?></td>
							<td><?php echo $prixL . " €"; ?></td>
							</tr>
						</tbody>
						<?php } ?>
					</table>
					<br>
					<table class="table table-bordered" id="tablePrix">
					<thead>
						<tr>
							<th>Prix TTC</th>
							<th><?php if($cpt>=10){
										echo round(($prixTotal*1.2)*0.9, 2);}
									else{
										echo round ($prixTotal*1.2, 2 );} ?> €</th>
						</tr>
					</thead>
					</table>
				</div>
				<?php
					if ($etat == 0){
				?>
				<form action="index.php?controle=facture&action=formPaye" method="post" enctype="multipart/form-data">
					<input name="idE" type="hidden" value="<?php echo $_SESSION['profil']['ide']; ?>">
					<input class="bt" type= "submit"  value="Payer" name="submit">
				</form>
				<?php } else { ?> <h1 style="text-align: center" >Facture Payée</h1><?php } ?>
			</div>
		<?php } else{ ?> <h1 style="text-align: center" >Vous n'avez rien loué</h1><?php }?>
	</body>
</html>
