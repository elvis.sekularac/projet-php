<!doctype html>
<html lang="fr">
	<head>
	</head>
	<body>
		<footer class="footer">
			<div class="container">
				<div class="row">
			    	<div class="col-sm">
				    	<h3>Les horaires</h3>
					    <li>Lun 10h-19h</li>
					    <li>Mar 10h-19h</li>
					    <li>Mer 10h-19h</li>
					    <li>Jeu 10h-19h</li>
					    <li>Ven 10h-19h</li>
					    <li>Sam fermé</li>
					    <li>Dim fermé</li><br>
			    	</div>
				    <div class="col-sm">
				    	<h3>Restons en contact</h3>
						<p>55-55-55-55-55</p>
						<p>supportclient@contact.com</p>
						<p>6 rue de l'invention, Parie VII, 75007</p>
				 	</div>
				 	<div class="col-sm">
				    	<h3>Les réseaux</h3>
					    <div class="row" id="reseaux">
						 	<li><a class="fa fa-twitter blanc" href="https://twitter.com/RentMyCar2" aria-hidden="true"></a></li>
						    <li><a class="fa fa-instagram blanc" href="https://www.instagram.com/rent_my_car_php/" aria-hidden="true"></a><li>
						    <li><a class="fa fa-facebook blanc" href="https://www.facebook.com/rentmy.car.129" aria-hidden="true"></a></li>
						</div>
				 	</div>
				</div>
			</div>
		</footer>
	</body>
</html>