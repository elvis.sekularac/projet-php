<html>
	<head>
		<meta charset="utf-8"/>
	</head>
	<body>
		<div id="formulaireVoiture">
			<h3> Location d'un véhicule </h3>
			<?php
				require_once("controle/voiture.php");
				$v=choix();
				$c= json_decode($v['caract']);
			?>
			<div class='card' id='cardL'>
				<img class='card-img-top' src='vue/image/<?php echo ($v['photo']); ?>'>
				<div class='card-body'>
					<p class='card-text'>Type : <?php echo ($v['type']); ?> </p>
					<p class='card-text'>Caractérisque : moteur : <?php echo ($c->moteur); ?> ; vitesse : <?php echo ($c->vitesse); ?> ; places : <?php echo ($c->places); ?> ; prix journalier : <?php echo ($c->prix); ?> €</p>
					<p class='card-text'>Etat : <?php echo ($v['location']); ?></p>
				</div>
			</div>
			<form action="index.php?controle=facture&action=location" method="post" enctype="multipart/form-data">
				<table>
					<tr>
						<td class="attVoiture">
							<label>Date de début :</label>
						</td>
						<td>
							<input 	name="dateD" type="date" min="2019-01-01" max="2025-12-31" value= "" />
						</td>
					</tr>
					<tr>
						<td class="attVoiture">
							<label>Date de fin :</label>
						</td>
						<td>
							<input 	name="dateF" type="date"  min="2019-01-01" max="2025-12-31" value= "" />
						</td>
					</tr>
				</table>
				<input name="idV" type="hidden" value="<?php echo ($v['idv']); ?>">
				<input name="valeur" type="hidden" value="<?php echo ($c->prix); ?>">
				<br>
				<input id="soumVoiture" type= "submit"  value="Soumettre" name="submit">
			</form>
		</div>
		<div align="center" color="red"> <?php if(isset($_SESSION['msg'])) echo $_SESSION['msg']; ?> </div>
	</body>
</html>