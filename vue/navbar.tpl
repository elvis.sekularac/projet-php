<html>
<body>

<?php
  if(isset($_SESSION['profil'])) {
     $bool = true;
     if($_SESSION['profil']['ide']=='0'){
        $admin = true;
     }
     else $admin = false;
   }
  else  $bool = false;
  if ($bool==true) $accueil = '\'index.php?controle=entreprise&action=connecte\''; else $accueil = '\'index.php?controle=entreprise&action=anonyme\'';

 ?>

<header class="container-fluid">
  <div class="row">
    <div class="col-md-4"></div>
    <div class="col-md-4"><h2 id="titreSite"> Rent My Car </h2></div>

    <?php 
    if($bool){ ?>

    <div class="col-md-2"></div>
    <div class="col-md-1"  id="connexionBloc">
        <div id="bonjour" > Bonjour <?php echo $_SESSION['profil']['nom']; ?></div>
    </div>
    <div class="col-md-1"  id="connexionBloc">
      <a id="connexionLogo" href='index.php?controle=entreprise&action=deconnexion'>
        <i class="fa fa-power-off"></i>
      </a>
    </div>

    <?php
     } else {?>

    <div class="col-md-3"></div>
    <div class="col-md-1"  id="connexionBloc">
      <a id="connexionLogo" href='index.php?controle=entreprise&action=connexion'>
        <div class="iconeCo"> &#xe08a;  </div>
      </a>
    </div>

    <?php
    } ?>

  </div>
</header>


  <nav class="navbar navbar-expand-lg navbar-light bg-light" id="navbar">
    <a class="navbar-brand menu" id="maison" href=<?php echo $accueil ?>>&#xe074;</a>
    <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarTogglerDemo02" aria-controls="navbarTogglerDemo02" aria-expanded="false" aria-label="Toggle navigation">
      <span class="navbar-toggler-icon"></span>
    </button>
<?php
    if($bool){
      if(!$admin){
      ?>
    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item active">
          <a class="nav-link menu" href="index.php?controle=voiture&action=locationEntreprise">Vos locations <span class="sr-only">(current)</span></a>
        </li>
        <li class="nav-item">
          <a class="nav-link menu" href="index.php?controle=facture&action=facturerE" tabindex="-1" aria-disabled="true">Vos factures</a>
        </li>
      </ul>
    </div>
  <?php }
  else{
    ?>

    <div class="collapse navbar-collapse" id="navbarTogglerDemo02">
      <ul class="navbar-nav mr-auto mt-2 mt-lg-0">
        <li class="nav-item">
          <a class="nav-link menu" href="index.php?controle=voiture&action=stock">Votre Stock</a>
        </li>
        <li class="nav-item">
          <a class="nav-link menu" href="index.php?controle=voiture&action=ajout" tabindex="-1" aria-disabled="true">Ajouter au stock</a>
        </li>
        <li class="nav-item">
          <a class="nav-link menu" href="index.php?controle=facture&action=facturerA" tabindex="-1" aria-disabled="true">Vos factures</a>
        </li>
      </ul>
    </div>
  <?php } } ?>
  </nav>


</body>




</html>
